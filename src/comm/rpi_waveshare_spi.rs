//! Interface to the WaveShare ePaper Display over SPI on a Raspberry Pi
//! 
//! 

use std::error::Error;
use std::thread;
use std::time::Duration;

use rppal::spi::{Bus, Mode, Segment, SlaveSelect, Spi};
use rppal::gpio::{Gpio, InputPin, OutputPin};


const RST_PIN   : u8 = 17;
const DC_PIN    : u8 = 25;
const CS_PIN    : u8 = 8;
const BUSY_PIN  : u8 = 24;



struct GpioConfig {
  reset: OutputPin, 
  dc: OutputPin,
  cs: OutputPin,
  busy: InputPin
}

pub struct WaveSharePiSpi {
  config: GpioConfig,
  spi: Spi
}


impl WaveSharePiSpi {

  pub fn init() -> WaveSharePiSpi {

    let gpio_inst = Gpio::new().unwrap();

    let config = GpioConfig {
      reset: gpio_inst.get(RST_PIN).unwrap().into_output(),
      dc: gpio_inst.get(DC_PIN).unwrap().into_output(),
      cs: gpio_inst.get(CS_PIN).unwrap().into_output(),
      busy: gpio_inst.get(BUSY_PIN).unwrap().into_input()
    };

    let mut spi = Spi::new(Bus::Spi0, SlaveSelect::Ss0, 4_000_000, Mode::Mode0).unwrap();

    return WaveSharePiSpi {
      config,
      spi
    };

  }


  pub fn reset(&mut self) -> () {

    self.config.reset.set_high();
    thread::sleep(Duration::from_millis(200));
    self.config.reset.set_low();
    thread::sleep(Duration::from_millis(10));
    self.config.reset.set_high();
    thread::sleep(Duration::from_millis(200));

  }

  pub fn send_command(&mut self, data: u8){
    self.config.dc.set_low();
    self.config.cs.set_low();
    // Write Byte
    self.spi.write(&[data]).unwrap();
    self.config.cs.set_high();

  }

  pub fn send_data(&mut self, data: u8){
    self.config.dc.set_high();
    self.config.cs.set_low();
    // Write Byte
    self.spi.write(&[data]).unwrap();
    self.config.cs.set_high();

  }

  pub fn read_busy(&mut self){
    while self.config.busy.is_low() {
      thread::sleep(Duration::from_millis(200));
    }
  }

}