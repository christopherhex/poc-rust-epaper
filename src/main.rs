mod waveshare;
mod comm;


use std::thread;
use std::time::Duration;

extern crate image;

const EPD_WIDTH : i32 = 176;
const EPD_HEIGHT : i32 = 264;

use waveshare::{WaveShare};
use image::{GenericImageView, ImageBuffer, RgbImage};

fn inittest(image: &Vec<u8>)  {

    println!("Before RaspiPaper");
    let mut raspi_paper = WaveShare::new(EPD_WIDTH, EPD_HEIGHT);
    println!("After RaspiPaper");
    raspi_paper.init();

    raspi_paper.clear_screen();

    println!("Before Display");
    raspi_paper.display(image);
    println!("After Display");

    thread::sleep(Duration::from_secs(10));

    println!("After Reset");
    raspi_paper.clear_screen();
    println!("Cleared screen");
  
}

fn main() {
    println!("Hello, world!");
    let img = image::open("data/epaper_test.png").unwrap();

    // Convert to grayscale
    let luma = img.into_luma();
    let raw = luma.into_raw();

    println!("Run Test Script");
    inittest(&raw);
}
